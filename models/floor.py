# -*- coding: utf-8 -*-

from odoo import models, fields, api


class FloorDetails(models.Model):
    _name = 'floor.details'
    _description = 'Floor Details'

    name = fields.Char(string="Floor Name", required=False)
    floor_area = fields.Float(string="Floor Area/Sq.Feet", required=False)
    apartments_room = fields.Integer(string="No:of Apartments/Rooms")
    property_id = fields.Many2one('property.master', string="Property ID")


