# -*- coding: utf-8 -*-
def verify_bite(*foods):
    # Create a dictionary to count the number of each food item
    food_counts = {}
    for food in foods:
        if food in food_counts:
            food_counts[food] += 1
        else:
            food_counts[food] = 1

    # Check if any food item count is less than 2
    for count in food_counts.values():
        if count < 2:
            return "Bite."

    # If all food items have a count of 2 or more, Scooby will not bite
    return "Not bite."


# Test the function with the given inputs
print(verify_bite("Pedigree", "Kitkat", "Kitkat", "chocolate", "chocolate", "chocolate", "Carrot"))  # Output: Bite.
print(verify_bite("Pedigree", "Pedigree", "Pedigree", "Horlicks", "Horlicks"))  # Output: Not bite.
