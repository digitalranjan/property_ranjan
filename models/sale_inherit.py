# -*- coding: utf-8 -*-

from odoo import models, fields, api


class SaleOrderLine(models.Model):
    ''' Sale Order Inherit '''
    _inherit = 'sale.order'

    property_id = fields.Many2one('property.master', string='Property')

    @api.onchange('property_id')
    def _onchange_property(self):
        for rec in self:
            if rec.property_id.property_manager_id:
                rec.partner_id = rec.property_id.property_manager_id.id
