# -*- coding: utf-8 -*-

from odoo import models, fields


class PropertyTag(models.Model):
    """Property Tag Details """
    _name = "property.tag"
    _description = "Property Tag"

    name = fields.Char(string="Name", required="1")
    color = fields.Integer(string="Color")
    active = fields.Boolean(string="Active", default=True, copy=False)