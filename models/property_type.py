# -*- coding: utf-8 -*-

from odoo import models, fields


class PropertyType(models.Model):
    """Property Type Details """
    _name = 'property.type'
    _description = "Property Type"

    name = fields.Char(string="Name", required="1")
    parent_type = fields.Selection([
        ('residential', 'Residential'),
        ('commercial', 'Commercial')],
        string='Parent Type', default='residential')
    active = fields.Boolean(string="Active", default=True, copy=False)