# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class PropertyMaster(models.Model):
    """Property Management Details """
    _name = 'property.master'
    _description = 'Property Management'

    name = fields.Char(string="Name", default="Golden Villa")
    image = fields.Image(string='Image')
    property_seq = fields.Char(string="Property Sequence", readonly=True)
    address = fields.Char(string="Address")
    street1 = fields.Char(string='Street1', required=0)
    street2 = fields.Char(string='Street1', required=0)
    city = fields.Char(string='City')
    pin_code = fields.Char(string='Pin Code')
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict',
                               domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', required=0)
    property_type = fields.Char(string="Property Type")
    property_manager_id = fields.Many2one('res.partner', string='Property Manager')
    phone = fields.Char(string='Phone')
    email = fields.Char(string='Email')
    website = fields.Char(string='Website', placeholder='e.g. www.example.com')
    tags = fields.Char(string='Tags', placeholder='e.g. tag1, tag2, tag3')
    total_floor_area = fields.Float(string="Total Floor Area(Sq.Feet)", compute='_compute_total_floor_area', store=True)
    total_apartment = fields.Integer(string="Total Apartments/Rooms", compute='_compute_total_apartments', store=True)
    floor_details_ids = fields.One2many('floor.details', 'property_id', string='Floor Details')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'Name Should Be Unique !')
    ]

    @api.model
    def create(self, vals):
        print(".........", self.env['ir.sequence'])
        vals['property_seq'] = self.env['ir.sequence'].next_by_code('property.master')
        return super(PropertyMaster, self).create(vals)

    @api.onchange('state_id')
    def _onchange_states(self):
        if self.state_id.country_id:
            self.country_id = self.state_id.country_id

    @api.onchange('phone')
    def validate_phone(self):
        for rec in self:
            if self.phone:
                if self.phone[0] != '+':
                    print("::::::::::",self.phone)
                    if rec.phone and len(self.phone) != 10 or not self.phone.isdigit():
                        raise ValidationError(_('Accept only 10 Digit Phone Number'))
                    else:
                        phone_number = "+" + str(self.country_id.phone_code) + " " + str(self.phone)
                        print("Phone", phone_number)
                        self.phone = phone_number

    @api.depends('floor_details_ids.floor_area')
    def _compute_total_floor_area(self):
        for record in self:
            record.total_floor_area = sum(record.floor_details_ids.mapped('floor_area'))

    @api.depends('floor_details_ids.apartments_room')
    def _compute_total_apartments(self):
        for record in self:
            record.total_apartment = sum(record.floor_details_ids.mapped('apartments_room'))
