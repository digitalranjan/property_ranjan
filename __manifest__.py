# -*- coding: utf-8 -*-

{
    'name': 'Ranjan Property',
    'version': '1.0',
    'summary': 'Final Test',
    'sequence': -1,
    'description': """Property Management""",
    'category': 'Property',
    'website': 'https://www.oddd.com',
    'depends': ['base', 'sale'],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/ir_sequence_data.xml',
        'report/sale_order_report_inherit.xml',
        'views/property_view.xml',
        'views/property_tag_view.xml',
        'views/property_type_view.xml',
        'views/sale_inherit_views.xml',

        'views/menu.xml',
    ],
    'demo': [],
    'installable': True,
    'auto_install': False,
    'license': 'LGPL-3',
}
